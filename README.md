# WaifuSynth

⚠️ Work in progress

Tacotron2/WaveGlow speech synthesis for waifus

## Example

![2B Example](example.mp3) ![2B Example 2](example2.mp3) ![2B Example 3](example3.mp3)

[2B pretrained model v1](https://www.mediafire.com/file/vjz09k062m02qpi/2b_v1.pt/file) (112.8 MB)

## To-do

- Simplify commandline interface
- Deploy pretrained models of various anime characters
- Improve model parameters
- Create Google Collab

## How it works

1. Use [Clipchan] to get voice clips of your waifu
2. Train Tacotron2 with them
3. Type whatever you want and Tacotron2 generates a mel spectrogram
4. WaveGlow converts mel spectrogram to your waifu speaking
5. ????
6. PROFIT!!

## Pre-requisites

### Audio synthesis

- Python 3.7 or Python 3.8
- [PyTorch](https://pytorch.org)
- Optional: if training, NVIDIA GPU with CUDA 9.2 (on Python 3.7) or CUDA 10.1 (on Python 3.8)

## Setup

### Clone the repo

```command
git clone https://gitlab.com/kokubunji/waifusynth.git
cd waifusynth
```

### Install requirements

Download the latest version of [PyTorch](https://pytorch.org/)

Example, to install PyTorch 1.6.0 for CPU:
```command
pip3 install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
```

#### Python 3.7

`pip3 install -r requirements_37.txt`

- Tensorflow 1.14 - 1.15
- Numpy 1.17 - 1.18
- Scipy 1.3.3

#### Python 3.8

`pip3 install -r requirements_38.txt`

- Tensorflow 2.3.0
- Numpy 1.18
- Scipy 1.4.1

#### Training requirements

If doing distributed training, install [Apex].

If using mixed precision training, install [AMP].

Note: [WaveGlow](https://github.com/NVIDIA/WaveGlow) and [Tacotron2](https://github.com/NVIDIA/tacotron2) may have upstream updates available.

## Tutorial

1. Install everything.
2. Download the published [Tacotron 2] model (108 MB) into the same folder with `quickstart.py`
3. Download the published [WaveGlow] model (644 MB) too

To quickly sample Tacotron2 + WaveGlow:
```command
python quickstart.py
```
Once the model is loaded it will ask for input in a loop, outputting audio clips to the `output` directory.

For further reading, read the [Tacotron2/Waveglow tutorial](https://pytorch.org/hub/nvidia_deeplearningexamples_tacotron2/).

## Acknowledgements

Speech synthesis code forked from [NVIDIA/WaveGlow](https://github.com/NVIDIA/WaveGlow) and [Tacotron2](https://github.com/NVIDIA/tacotron2).

## Tacotron2

PyTorch implementation of [Natural TTS Synthesis By Conditioning
Wavenet On Mel Spectrogram Predictions](https://arxiv.org/pdf/1712.05884.pdf). 

This implementation includes **distributed** and **automatic mixed precision** support
and uses the [LJSpeech dataset](https://keithito.com/LJ-Speech-Dataset/).

Distributed and Automatic Mixed Precision support relies on NVIDIA's [Apex] and [AMP].

Visit the [website] for audio samples using the published [Tacotron 2] and
[WaveGlow] models.

### Setup
Update .wav paths: `sed -i -- 's,DUMMY,ljs_dataset_folder/wavs,g' filelists/*.txt`
    - Alternatively, set `load_mel_from_disk=True` in `hparams.py` and update mel-spectrogram paths

### Training a new model from scratch

1. Increase learning rate to 1e-3 on line 81 of `hparams.py`.
2. `python tacotron2_train.py --output_directory=outdir --log_directory=logdir`
3. Set the batch size to an appropriate size for your card on line 310 in `hparams.py`
4. (OPTIONAL) `tensorboard --logdir=outdir/logdir`

When the training loss plateaus halve the learning rate.

### Training using a pre-trained model
Training using a pre-trained model can lead to faster convergence. By default, the dataset dependent text embedding layers are [ignored].

1. Download the published [Tacotron 2] model (108 MB)
2. Set the batch size to an appropriate size for your card on line 310 in `hparams.py`
3. `python tacotron2_train.py --output_directory=outdir --log_directory=logdir -c tacotron2_statedict.pt --warm-start`
4. (OPTIONAL) `tensorboard --logdir=outdir/logdir`

When the training loss plateaus halve the learning rate.

#### Using your trained model

```
python quickstart.py -t outdir/checkpoint_1000
```

### Training considerations

**Recommended batch size for a 6 GB card**: 12

To use your trained model's first checkpoint after 1000 iterations with `quickstart.py`:

```
python quickstart.py --tacotron2 outdir/checkpoint_1000
```

To redistribute your model, make sure to remove 'optimizer' from the checkpoint file. It's only necessary for resuming training and is twice the size of the trained model.

### Multi-GPU (distributed) and Automatic Mixed Precision Training
1. `python -m multiproc tacotron2_train.py --output_directory=outdir --log_directory=logdir --hparams=distributed_run=True,fp16_run=True`

### Inference demo
1. Download the published [Tacotron 2] model (108 MB)
2. Download the published [WaveGlow] model (644 MB)
3. `jupyter notebook --ip=127.0.0.1 --port=31337`
4. Load `inference.ipynb `

N.b.  When performing Mel-Spectrogram to Audio synthesis, make sure Tacotron 2
and the Mel decoder were trained on the same mel-spectrogram representation. 

## WaveGlow

In a recent [paper] by Ryan Prenger, Rafael Valle, and Bryan Catanzaro, they
proposed WaveGlow: a flow-based network capable of generating high quality speech
from mel-spectrograms. WaveGlow combines insights from [Glow] and [WaveNet] in order
to provide fast, efficient and high-quality audio synthesis, without the need for
auto-regression. WaveGlow is implemented using only a single network, trained using
only a single cost function: maximizing the likelihood of the training data, which
makes the training procedure simple and stable.

Their [PyTorch] implementation produces audio samples at a rate of 1200 
kHz on an NVIDIA V100 GPU. Mean Opinion Scores show that it delivers audio
quality as good as the best publicly available WaveNet implementation.

Visit their [website] for audio samples.

### Generate audio with pre-existing model

1. Download the published [Tacotron 2] model (108 MB)
2. Download the published [WaveGlow] model (644 MB)
2. Download [mel-spectrograms] (1.6 MB)
3. Generate audio `python3 inference.py -f <(ls mel_spectrograms/*.pt) -w waveglow_256channels.pt -o . --is_fp16 -s 0.6`  

N.b. use `convert_model.py` to convert your older models to the current model
with fused residual and skip connections.

### Train your own model

1. Download [LJ Speech Data] (2.6 GB). In this example it's in `data`

2. Make a list of the file names to use for training/testing

```command
ls data/*.wav | tail -n+10 > train_files.txt
ls data/*.wav | head -n10 > test_files.txt
```

3. Train your WaveGlow networks

```command
mkdir checkpoints
python train.py -c config.json
```

   For multi-GPU training replace `train.py` with `distributed.py`.  Only tested with single node and NCCL.

   For mixed precision training set `"fp16_run": true` on `config.json`.

4. Make test set mel-spectrograms

`python mel2samp.py -f test_files.txt -o . -c config.json`

5. Do inference with your network

```command
ls *.pt > mel_files.txt
python3 inference.py -f mel_files.txt -w checkpoints/waveglow_10000 -o . --is_fp16 -s 0.6
```

[Clipchan]: https://gitlab.com/kokubunji/clipchan
[pytorch 1.0]: https://github.com/pytorch/pytorch#installation
[website]: https://nv-adlr.github.io/WaveGlow
[paper]: https://arxiv.org/abs/1811.00002
[WaveNet implementation]: https://github.com/r9y9/wavenet_vocoder
[Glow]: https://blog.openai.com/glow/
[WaveNet]: https://deepmind.com/blog/wavenet-generative-model-raw-audio/
[PyTorch]: http://pytorch.org
[published model]: https://drive.google.com/open?id=1rpK8CzAAirq9sWZhe9nlfvxMF1dRgFbF
[mel-spectrograms]: https://drive.google.com/file/d/1g_VXK2lpP9J25dQFhQwx7doWl_p20fXA/view?usp=sharing
[LJ Speech Data]: https://keithito.com/LJ-Speech-Dataset
[Apex]: https://github.com/nvidia/apex
[WaveGlow]: https://drive.google.com/open?id=1rpK8CzAAirq9sWZhe9nlfvxMF1dRgFbF
[Tacotron 2]: https://drive.google.com/file/d/1c5ZTuT7J08wLUoVZ2KkUs_VdZuJ86ZqA/view?usp=sharing
[ignored]: https://github.com/NVIDIA/tacotron2/blob/master/hparams.py#L22
[AMP]: https://github.com/NVIDIA/apex/tree/master/apex/amp
