import os
from argparse import ArgumentParser
import torch
import numpy as np
from scipy.io.wavfile import write
from collections import Counter
import re
import time

p = ArgumentParser()
p.add_argument("--output", "-o", default="output", help="wav output directory")
p.add_argument("--waveglow", "-w", default="waveglow_256channels_universal_v5.pt", help="waveglow model")
p.add_argument("--tacotron2", "-t", default="tacotron2_statedict.pt", help="tacotron2 model state dict")
p.add_argument("--device", "-d", type=str, default="cpu", help="device to use, cpu or cuda")
p.add_argument("--debug", "-D", action="store_true")
args = p.parse_args()

print("Loading, please wait...")

# Turn off warnings
import warnings
import logging
if not args.debug:
    warnings.filterwarnings("ignore")
    logging.getLogger('tensorflow').disabled = True

# Tacotron2
import hparams
from model import Tacotron2
import text

# WaveGlow
import json
from glow import WaveGlow
from load import waveglow_compatability_load # for loading old torch models

# Create output directory
if not os.path.exists(args.output):
    os.mkdir(args.output)

device = torch.device(args.device)

# Load Tacotron 2
if args.debug: print("Loading Tacotron2...")
tacotron2_statedic = torch.load(args.tacotron2)['state_dict']
tacotron2_hparams = hparams.create_hparams()
if device.type == 'cpu':
    tacotron2_hparams.cuda_enabled = False
else:
    tacotron2_hparams.cuda_enabled = True
tacotron2 = Tacotron2(tacotron2_hparams).to(device)
tacotron2.load_state_dict(tacotron2_statedic)
tacotron2.eval()

# Load WaveGlow
if args.debug: print("Loading WaveGlow...")
#waveglow = torch.load(args.waveglow)['model']
#n_mel_channels, n_flows, n_group, n_early_every, n_early_size, WN_config
config = json.load(open('config.json'))
waveglow_config = config["waveglow_config"]
waveglow = waveglow_compatability_load(args.waveglow, WaveGlow, waveglow_config).to(device)
waveglow = waveglow.remove_weightnorm(waveglow)
waveglow.eval()

user_input = 'null'
count = Counter()
while user_input != "":
    user_input = input("Input: ")
    count[user_input] += 1
    
    # preprocessing
    start = time.time()
    sequence = np.array(text.text_to_sequence(user_input, ['english_cleaners']))[None, :]
    sequence = torch.from_numpy(sequence).to(device=device, dtype=torch.int64)

    # run the models
    with torch.no_grad():
        _, mel, _, _ = tacotron2.inference(sequence)
        audio = waveglow.infer(mel)
    audio_numpy = audio[0].data.cpu().numpy()
    rate = 22050

    filename = os.path.join(args.output, "{:03}-{}.wav".format(count[user_input], re.sub("[?!.,:]", "", user_input)))
    write(filename, rate, audio_numpy)
    print("  Created {} in {:.1f}s".format(filename, time.time() - start))
